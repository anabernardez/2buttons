class Button {
  constructor(x, y, width, height, text) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.text = text;
      this.active = false;
      this.inputText = '';
  }

  draw(ctx) {
      ctx.fillStyle = this.active ? 'lightblue' : 'lightgrey';
      ctx.fillRect(this.x, this.y, this.width, this.height);
      ctx.strokeRect(this.x, this.y, this.width, this.height);
      ctx.fillStyle = 'black';
      ctx.font = '16px Arial';
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.fillText(this.active ? this.inputText : this.text, this.x + this.width / 2, this.y + this.height / 2);
  }

  isClicked(mouseX, mouseY) {
      return mouseX >= this.x && mouseX <= this.x + this.width && mouseY >= this.y && mouseY <= this.y + this.height;
  }

  isHovered(mouseX, mouseY) {
      return this.isClicked(mouseX, mouseY);
  }

  activate() {
      this.active = true;
      this.inputText = '';
  }

  deactivate() {
      this.active = false;
  }

  addChar(char) {
      this.inputText += char;
  }

  removeChar() {
      this.inputText = this.inputText.slice(0, -1);
  }

  getText() {
      return this.inputText;
  }
}

class Controller {
  constructor(canvasId) {
      this.canvas = document.getElementById(canvasId);
      this.ctx = this.canvas.getContext('2d');
      this.buttons = [];
      this.activeButton = null;
      this.init();
  }

  init() {
      this.canvas.addEventListener('click', (event) => this.handleClick(event));
      this.canvas.addEventListener('mousemove', (event) => this.handleMousemove(event));
      document.addEventListener('keydown', (event) => this.handleKeydown(event));
  }

  addButton(button) {
      this.buttons.push(button);
      button.draw(this.ctx);
  }

  handleClick(event) {
      const rect = this.canvas.getBoundingClientRect();
      const mouseX = event.clientX - rect.left;
      const mouseY = event.clientY - rect.top;

      this.buttons.forEach(button => {
          if (button.isClicked(mouseX, mouseY)) {
              if (this.activeButton) {
                  this.activeButton.deactivate();
                  this.activeButton.draw(this.ctx);
              }
              button.activate();
              this.activeButton = button;
          } else {
              button.deactivate();
          }
          button.draw(this.ctx);
      });
  }

  handleMousemove(event) {
      const rect = this.canvas.getBoundingClientRect();
      const mouseX = event.clientX - rect.left;
      const mouseY = event.clientY - rect.top;

      let hovered = false;
      this.buttons.forEach(button => {
          if (button.isHovered(mouseX, mouseY)) {
              this.canvas.style.cursor = 'text';
              hovered = true;
          }
      });
      if (!hovered) {
          this.canvas.style.cursor = 'default';
      }
  }

  handleKeydown(event) {
      if (this.activeButton) {
          if (event.key === 'Enter') {
              console.log(`Texto ingresado: ${this.activeButton.getText()}`);
              this.activeButton.deactivate();
              this.activeButton.draw(this.ctx);
              this.activeButton = null;
          } else if (event.key === 'Backspace') {
              this.activeButton.removeChar();
              this.clearCanvas();
              this.redrawButtons();
          } else if (/^[\p{L}\p{N}\p{P}\p{S}\s]$/u.test(event.key)) { // regex to match letters, numbers, punctuation, symbols, and spaces
              this.activeButton.addChar(event.key);
              this.clearCanvas();
              this.redrawButtons();
          }
          event.preventDefault(); // Prevent default behavior for controlled keys
      }
  }

  clearCanvas() {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  redrawButtons() {
      this.buttons.forEach(button => button.draw(this.ctx));
  }
}

// Crear una instancia del controlador y agregar botones
const controller = new Controller('myCanvas');
controller.addButton(new Button(50, 50, 200, 50, 'Click to type'));
controller.addButton(new Button(50, 150, 200, 50, 'Click to type'));
